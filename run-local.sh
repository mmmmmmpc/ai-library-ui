#!/bin/bash

source secrets

export PORT=$PORT
export IP=$IP

export SENTIMENT_ANALYSIS_URL="$SENTIMENT_ANALYSIS_URL"
export SENTIMENT_ANALYSIS_USERNAME="$SENTIMENT_ANALYSIS_USERNAME"
export SENTIMENT_ANALYSIS_PASSWORD="$SENTIMENT_ANALYSIS_PASSWORD"

export FLAKE_ANALYSIS_URL="$FLAKE_ANALYSIS_URL"

npm run dev