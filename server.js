"use strict";

const Hapi = require("hapi");
const HapiPino = require("hapi-pino");
const Inert = require("inert");
const Wreck = require("wreck");
const urlJoin = require("url-join");

const PORT = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080;
const IP = process.env.IP || process.env.OPENSHIFT_NODEJS_IP || "0.0.0.0";
const SENTIMENT_ANALYSIS_URL = process.env.SENTIMENT_ANALYSIS_URL;
const FLAKE_ANALYSIS_URL = process.env.FLAKE_ANALYSIS_URL;

if (!SENTIMENT_ANALYSIS_URL) {
  console.error("SENTIMENT_ANALYSIS_URL not set")
}

const SENTIMENT_ANALYSIS_BASIC_AUTH_HEADER = getBasicAuthHeader();

const STATUS = {
  status: "OK",
  started: new Date(),
  sentimentAnalysisUrl: SENTIMENT_ANALYSIS_URL,
  flakeAnalysisUrl: FLAKE_ANALYSIS_URL
};


const server = Hapi.server({
  port: PORT,
  host: IP
});

const init = async () => {
  await server.register(Inert);
  await server.register(HapiPino);

  server.route({
    method: "GET",
    path: "/api/status",
    handler: (request, h) => {
      return STATUS;
    }
  });

  server.route({
    method: ["GET", "PUT", "POST", "PATCH", "DELETE", "OPTIONS"],
    path: "/api/sentiment/{params*}",
    handler: (request, h) => {
      return proxyApiRequest(
        request,
        h,
        SENTIMENT_ANALYSIS_URL,
        {Authorization: SENTIMENT_ANALYSIS_BASIC_AUTH_HEADER});
    }
  });

  server.route({
    method: "GET",
    path: "/{path*}",
    handler: {
      directory: {
        path: "ui/build"
      }
    }
  });

  server.ext("onPreResponse", (request, h) => {
    const response = request.response;
    const isNotApi = !/^\/api/g.exec(request.url.path);
    if (isNotApi && response.isBoom &&
      response.output.statusCode === 404) {
      return h.file("ui/build/index.html");
    }

    return h.continue;
  });

  await server.start();
  console.log(`Server running at: ${server.info.uri}`);
};

process.on("unhandledRejection", (err) => {
  console.log(err);
  process.exit(1);
});

init();

function getBasicAuthHeader() {
  const username = process.env.SENTIMENT_ANALYSIS_USERNAME || "";
  const password = process.env.SENTIMENT_ANALYSIS_PASSWORD || "";

  if (!username) {
    console.error("SENTIMENT_ANALYSIS_USERNAME environment variable missing.  Basic Auth username for sentiment analysis not set")
  }

  if (!username) {
    console.error("SENTIMENT_ANALYSIS_PASSWORD environment variable missing.  Basic Auth password for sentiment analysis not set")
  }

  let authStr = Buffer.from(`${username}:${password}`).toString("base64");
  return `Basic ${authStr}`;
}


function proxyApiRequest(request, h, serviceUrl, extraHeaders = {}) {
  const apiRequestUrl = urlJoin(serviceUrl, request.params.params || "", request.url.search || "");
  let headers = {...request.headers, ...extraHeaders};
  delete headers.host;
  delete headers["content-length"];

  return Wreck.request(
    request.method,
    apiRequestUrl,
    {
      payload: request.payload,
      headers: headers,
      rejectUnauthorized: false
    });
}