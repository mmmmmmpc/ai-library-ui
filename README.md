# AI Library UI

### Deploying the Web App
First edit a `secrets` file similar to the `secrets.example`, but with your personal information.  Then, you can create the UI app using the included script.
```bash
./openshift-deploy.sh
```

Instead of a secrets file, you can also just use the template directly and replace `<VAR>` with your values.
```
oc process -f openshift/ai-library-ui.yml \
  -p SENTIMENT_ANALYSIS_URL=<SENTIMENT_ANALYSIS_URL> \
  -p SENTIMENT_ANALYSIS_USERNAME=<SENTIMENT_ANALYSIS_USERNAME> \
  -p SENTIMENT_ANALYSIS_PASSWORD=<SENTIMENT_ANALYSIS_PASSWORD> \
  -p FLAKE_ANALYSIS_URL=<FLAKE_ANALYSIS_URL> \
  | oc create -f -
```
