#!/bin/bash
set -x

source secrets

oc process -f openshift/ai-library-ui.yml \
  -p SENTIMENT_ANALYSIS_URL=${SENTIMENT_ANALYSIS_URL} \
  -p SENTIMENT_ANALYSIS_USERNAME=${SENTIMENT_ANALYSIS_USERNAME} \
  -p SENTIMENT_ANALYSIS_PASSWORD=${SENTIMENT_ANALYSIS_PASSWORD} \
  -p FLAKE_ANALYSIS_URL=${FLAKE_ANALYSIS_URL} \
  | oc create -f -
