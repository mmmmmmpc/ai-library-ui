import { combineReducers } from "redux";
import { statusReducer } from "./Status/reducers";
import { homeReducer } from "./Home/reducers";
import { notificationsReducer } from "./Notifications/reducers";
import { sentimentReducer } from "./Sentiment/reducers";
import { reducer as formReducer } from "redux-form";

const rootReducer = combineReducers({
  statusReducer: statusReducer,
  homeReducer: homeReducer,
  notificationsReducer: notificationsReducer,
  sentimentReducer: sentimentReducer,
  form: formReducer
});

export default rootReducer;
