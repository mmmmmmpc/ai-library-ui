import * as React from "react";

import { Icon } from "patternfly-react"

import { CopyToClipboard } from "react-copy-to-clipboard";
import ReactJson from "react-json-view";
import "./JsonSample.css"


class JsonSample extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showJson: !props.initHidden
    }
  };

  toggleDetails = () => {
    this.setState({showJson: !this.state.showJson})
  };

  render() {
    const {title, object} = this.props;
    const { showJson } = this.state;

    const toggleDetailsText = showJson
      ? <span><Icon type="fa" name="chevron-down"/> Hide</span>
      : <span><Icon type="fa" name="chevron-right"/> Show</span>;
    const jsonSection = showJson ?
      <ReactJson
        name={false}
        enableClipboard={false}
        onEdit={false}
        onAdd={false}sentimentItem
        onDelete={false}
        displayDataTypes={false}
        src={object}
      />
      : "";

    return (
      <div className="JsonSample json-sample-section">
        <div className="json-sample-heading">
          <h3 className="json-sample-title">{title}</h3>
          <CopyToClipboard
            text={JSON.stringify(object, undefined, 2)}>
            <a className="clickable-empty-link json-sample-action"><Icon type="fa" name="copy"/> Copy</a>
          </CopyToClipboard>
          <a className="clickable-empty-link json-sample-action" onClick={this.toggleDetails}>{toggleDetailsText}</a>
        </div>
        {jsonSection}
      </div>
    )
  }
}

export default JsonSample;

