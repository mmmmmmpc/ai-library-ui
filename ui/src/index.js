import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";
import { createStore, applyMiddleware, compose } from "redux";
import { Provider } from "react-redux";
import thunkMiddleware from "redux-thunk";

import rootReducer from "./reducers";

import "../node_modules/patternfly/dist/css/patternfly.css";
import "../node_modules/patternfly/dist/css/patternfly-additions.css";
import "../node_modules/patternfly-react/dist/css/patternfly-react.css";
import "../node_modules/patternfly-react-extensions/dist/css/patternfly-react-extensions.css";
import "../node_modules/@fortawesome/fontawesome-free/css/all.css";
import "./index.css";
import App from "./App";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunkMiddleware))
);

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <App/>
    </Router>
  </Provider>,
  document.getElementById("root"));

