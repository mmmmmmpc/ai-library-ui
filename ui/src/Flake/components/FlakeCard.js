import React from "react";
import {
  Button,
  Icon,
  Wizard
} from "patternfly-react";
import ServiceTile from "../../ServiceTile/components/ServiceTile"

import "./FlakeCard.css"
import { connect } from "react-redux";


class FlakeCard extends React.Component {
  state = {
    showModal: false
  };

  cardClick = e => {
    e.preventDefault();
    this.openWizard();
  };

  openWizard = () => {
    this.setState({
      showModal: true,
      activeStepIndex: 0
    });
  };

  closeWizard = () => {
    this.setState({showModal: false})
  };


  render() {
    const {status} = this.props;
    const {showModal} = this.state;
    return (
      <div>
        <ServiceTile
          key="tile-flake"
          title="Flake Analysis"
          featured={true}
          iconClass="fa fa-snowflake-o"
          description="Identify false positives."
          onClick={this.cardClick}
        />
        <Wizard show={showModal} onHide={this.closeWizard} className="flake-wizard service-wizard" backdrop="static">
          <Wizard.Header
            onClose={this.closeWizard}
            title={<span><span className="service-wizard-title-icon fa fa-snowflake-o"/> Flake Analysis</span>}
          />
          <Wizard.Body>
            <Wizard.Row>
              <Wizard.Main>
                <div className="flake-info">
                  <h2>What is Flake Analysis?</h2>
                  <p>Flake analysis aims to identify "flakes" or false positives among failures in a testing system.
                    By analyzing past failures, and whether or not they were ignored, we can train a machine
                    model to recognize similar failures that should be ignored.</p>
                  <p>
                    Read more on the related <a href={status.flakeAnalysisUrl}
                                                target="_blank">blog post <Icon type="fa" name="external-link-alt"/></a>.
                  </p>
                </div>
              </Wizard.Main>
            </Wizard.Row>
          </Wizard.Body>
          <Wizard.Footer>
            <Button
              bsStyle="default"
              className="btn-cancel"
              onClick={this.closeWizard}
            >
              Close
            </Button>
          </Wizard.Footer>
        </Wizard>
      </div>
    );
  };
}


function mapStateToProps(state) {
  return {
    ...state.statusReducer
  };
}

export default connect(mapStateToProps)(FlakeCard);

