import * as React from "react";
import { connect } from "react-redux";

import {
  Icon,
  Grid
} from "patternfly-react";

import {
  CatalogTileView
} from "patternfly-react-extensions";

import ServiceTile from "../../ServiceTile/components/ServiceTile"
import SentimentCard from "../../Sentiment/components/SentimentCard";
import FlakeCard from "../../Flake/components/FlakeCard";

import "./Home.css"

const MOCK_SERVICES = [
  {
    icon: {name: "zone", type: "pficon"},
    label: "clustering",
    title: "Clustering",
    description: "Discover more about my data structure and relationships"
  },
  {
    icon: {name: "exclamation-circle", type: "fa"},
    label: "anomaly",
    title: "Anomaly Detection",
    description: "Discover unique/outlier data points"
  },
  {
    icon: {name: "line-chart", type: "fa"},
    label: "regression",
    title: "Regression",
    description: "Use my data to predict future outcomes and values"
  },
  {
    icon: {name: "sitemap", type: "fa"},
    label: "multi-class",
    title: "Multi-Class Classification",
    description: "Categorize my data into MORE THAN 2 buckets"
  },
  {
    icon: {name: "share-alt", type: "fa"},
    label: "two-class",
    title: "Two Classification",
    description: "Categorize my data into only 2 buckets"
  },
  {
    icon: {name: "robot", type: "fa"},
    label: "bots",
    title: "Bots",
    description: "Add an agent to my workflow that reacts to events and executes a task"
  },
  {
    icon: {name: "laptop-code", type: "fa"},
    label: "custom",
    title: "Custom",
    description: "Insert my own code into an event"
  },
];

class Home extends React.Component {
  render() {
    let mockCards = MOCK_SERVICES.map((s, index) => (
      <ServiceTile
        key={`tile-${index}`}
        title={s.title}
        featured={false}
        comingSoon={true}
        iconClass={`${s.icon.type} ${s.icon.type}-${s.icon.name}`}
        description={s.description}
        onClick={e => console.log(`${s.title} clicked`)}
      />
    ));

    return (
      <div className="Home">
        <div className="ai-library-desc-panel">
          <Grid>
            <Grid.Row>
              <Grid.Col xs={12}>
                <h1>AI Library</h1>
                <p>
                  The AI Library provides machine learning as a service. The development of these services is a
                  community driven open source project to make AI more accessible. You can try out some of those
                  services here.
                </p>
                <p>
                  Learn more about the AI Library at the
                  <a target="_blank" rel="noopener noreferrer" href="https://gitlab.com/opendatahub/ai-library"> project page <Icon type="fa" name="external-link-alt"/></a>.
                </p>
              </Grid.Col>
            </Grid.Row>
          </Grid>
        </div>
        <Grid>
          <Grid.Row>
            <Grid.Col xs={12}>
              <CatalogTileView>
                <CatalogTileView.Category
                  title=""
                  totalItems={MOCK_SERVICES.length}
                  viewAll={true}
                  onViewAll={() => console.log("View All")}
                >
                  <SentimentCard/>
                  <FlakeCard/>
                  {mockCards}
                </CatalogTileView.Category>
              </CatalogTileView>
            </Grid.Col>
          </Grid.Row>
        </Grid>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return state.homeReducer;
}

export default connect(mapStateToProps)(Home);
