import * as d3 from "d3";
import cloud from "d3-cloud";

let d3WordCloud = {};

const defaultConfig = {
  width: 600,
  height: 300,
  textFn: word => word.text,
  sizeFn: word => 10 + Math.random() * 90,
  colorFn: word => "#363636",
  rotationFn: word => 0
};

d3WordCloud.create = (el, data, configOverrides = {}) => {
  const config = {...defaultConfig, ...configOverrides};

  let layout = cloud()
    .size([config.width, config.height])
    .words(data.map(d => ({
      text: config.textFn(d),
      size: config.sizeFn(d),
      color: config.colorFn(d),
      rotate: config.rotationFn(d)
    })))
    .padding(5)
    .rotate(d => d.rotate)
    .font("Impact")
    .fontSize(d => d.size)
    .on("end", draw);

  layout.start();

  function draw(words) {
    d3.select(el).select("svg").remove();

    d3.select(el).append("svg")
      .attr("viewBox", `0 0 ${layout.size()[0]} ${layout.size()[1]}`)
      .attr("perserveAspectRatio","xMinYMid")
      .append("g")
      .attr("transform", "translate(" + layout.size()[0] / 2 + "," + layout.size()[1] / 2 + ")")
      .selectAll("text")
      .data(words)
      .enter().append("text")
      .style("fill", d => d.color)
      .style("font-size", d=> d.size + "px")
      .style("font-family", "Impact")
      .attr("text-anchor", "middle")
      .attr("transform", d => "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")")
      .text(d => d.text);
  }
};

d3WordCloud.update = (el, data, configuration, chart) => {
  // TODO: update the chart
};

d3WordCloud.destroy = () => {
  // TODO: Cleaning code here
};


export default d3WordCloud;