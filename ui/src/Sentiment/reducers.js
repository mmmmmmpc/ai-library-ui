import {
  CREATE_SENTIMENT_PENDING, CREATE_SENTIMENT_FULFILLED, CREATE_SENTIMENT_REJECTED
} from "./actions";

const initialState = {
  sentiment: null,
  sentimentResponse: null,
  sentimentLoading: false,
  sentimentError: null
};

export const sentimentReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_SENTIMENT_PENDING:
      return {
        ...state,
        sentiment: null,
        sentimentResponse: null,
        sentimentLoading: true,
        sentimentError: null
      };
    case CREATE_SENTIMENT_FULFILLED:
      return {
        ...state,
        sentiment: action.payload.response.data.sentiment,
        sentimentResponse: action.payload.response.data,
        sentimentLoading: false,
        sentimentError: null,
      };
    case CREATE_SENTIMENT_REJECTED:
      console.error(action.payload.error);
      return {
        ...state,
        sentiment: null,
        sentimentResponse: null,
        sentimentLoading: false,
        sentimentError: action.payload.error
      };
    default:
      return state;
  }
};
