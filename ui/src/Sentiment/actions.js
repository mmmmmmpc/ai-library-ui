import axios from "axios"
import qs from "qs";
import { createAxiosErrorNotification } from "../Notifications/actions";

export const CREATE_SENTIMENT_PENDING = "CREATE_SENTIMENT_PENDING";
export const createSentimentPending = () => ({
  type: CREATE_SENTIMENT_PENDING,
  payload: {}
});

export const CREATE_SENTIMENT_FULFILLED = "CREATE_SENTIMENT_FULFILLED";
export const createSentimentFulfilled = (response) => ({
  type: CREATE_SENTIMENT_FULFILLED,
  payload: {
    response
  }
});

export const CREATE_SENTIMENT_REJECTED = "CREATE_SENTIMENT_REJECTED";
export const createSentimentRejected = (error) => ({
  type: CREATE_SENTIMENT_REJECTED,
  payload: {
    error
  }
});

export const createSentiment = (params, body) => {
  let paramStr = qs.stringify(params);
  paramStr = paramStr ? `?${paramStr}` : "";
  let url = `/api/sentiment/namespaces/_/actions/sentiment/ner-sentiment-svc${paramStr}`;
  return function (dispatch) {
    dispatch(createSentimentPending());
    return axios.post(url, body)
      .then(response => dispatch(createSentimentFulfilled(response)))
      .catch(error => {
        dispatch(createAxiosErrorNotification(error));
        dispatch(createSentimentRejected(error));
      })
  }
};