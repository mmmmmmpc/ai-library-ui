import * as React from "react";

import {
  Grid,
  Tab,
  Tabs
} from "patternfly-react";

import { calculateSentiment, generateSampleCurl } from "../utilities";

import SentimentWordCloud from "./SentimentWordCloud";
import SentimentLegend from "./SentimentLegend";
import SentimentDataTable from "./SentimentDataTable";
import AxiosError from "../../AxiosError/components/AxiosError";
import CodeSample from "../../CodeSample/components/CodeSample";
import JsonSample from "../../JsonSample/components/JsonSample";

class SentimentWizardResults extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tabActiveKey: 0
    }
  }

  selectTab = (tabActiveKey) => {
    this.setState({tabActiveKey});
  };

  renderError = () => {
    return <AxiosError error={this.props.sentimentError}/>
  };

  renderPending = () => {
    return (
      <div className="sentiment-wizard-contents sentiment-results sentiment-pending">
        <div>
          <span className="spinner spinner-xl spinner-inline"/>
        </div>
      </div>
    )
  };

  renderResults = () => {
    const maxSize = 60;
    const {sentiment, sentimentAnalysisUrl, sentimentText, sentimentResponse} = this.props;
    const {tabActiveKey} = this.state;
    const maxCount = Math.max(...sentiment.map(s => s.count));
    const baseSize = Math.round(maxSize / (Math.sqrt(maxCount)));

    let wordCloudConfig = {
      textFn: sentimentItem => sentimentItem.name,
      sizeFn: sentimentItem => (Math.sqrt(sentimentItem.count)) * baseSize,
      colorFn: sentimentItem => {
        switch (calculateSentiment(sentimentItem)) {
          case "verypositive":
            return "#3f9c35";
          case "positive":
            return "#0088ce";
          case "neutral":
            return "#8b8d8f";
          case "negative":
            return "#ec7a08";
          default:
            return "#cc0000"
        }
      }
    };

    const sampleCurl = generateSampleCurl(sentimentAnalysisUrl, sentimentText);

    return (
      <div className="sentiment-wizard-contents sentiment-results sentiment-success">
        <Tabs
          activeKey={tabActiveKey}
          onSelect={this.selectTab}
          animation={false}
          id="sentimentResultsTabs"
        >
          <Tab eventKey={0} title="Visual">
            <SentimentWordCloud
              data={sentiment}
              config={wordCloudConfig}
            />
            <SentimentLegend/>
            <SentimentDataTable data={sentiment}/>
          </Tab>
          <Tab eventKey={1} title="Data View">
            <Grid.Row>
              <Grid.Col className="sample-request-col" lg={6}>
                <CodeSample title="Sample Request" code={sampleCurl} language="bash"/>
              </Grid.Col>
              <Grid.Col className="sample-response-col" lg={6}>
                <JsonSample title="Sample Response" object={sentimentResponse}/>
              </Grid.Col>
            </Grid.Row>
          </Tab>
        </Tabs>

      </div>
    );
  };

  renderNoResults = () => {
    return (
      <div className="sentiment-wizard-contents sentiment-results sentiment-no-results">
        <h3>No Results</h3>
      </div>
    );
  };

  render() {
    const {sentiment, sentimentLoading, sentimentError} = this.props;

    if (sentimentError) {
      return this.renderError();
    } else if (sentimentLoading) {
      return this.renderPending();
    } else if (sentiment && sentiment.length) {
      return this.renderResults();
    }
    return this.renderNoResults();
  };
}

export default SentimentWizardResults;

