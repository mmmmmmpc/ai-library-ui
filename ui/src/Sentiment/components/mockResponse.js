const mockResponse = {
  data: {
    "sentiment": [{
      "category": ["PERSON"],
      "count": 1,
      "name": "Hoover",
      "sentiment": {"negative": 1}
    }, {
      "category": ["PERSON"],
      "count": 1,
      "name": "Chief Justice",
      "sentiment": {"negative": 1}
    }, {"category": ["NORP"], "count": 1, "name": "Americans", "sentiment": {"negative": 1}}, {
      "category": ["ORG"],
      "count": 2,
      "name": "Nation",
      "sentiment": {"negative": 2}
    }, {"category": ["ORG"], "count": 1, "name": "Nature", "sentiment": {"positive": 1}}, {
      "category": ["ORG"],
      "count": 1,
      "name": "Plenty",
      "sentiment": {"positive": 1}
    }, {"category": ["ORG"], "count": 1, "name": "This Nation", "sentiment": {"neutral": 1}}, {
      "category": ["GPE"],
      "count": 1,
      "name": "Hand",
      "sentiment": {"negative": 1}
    }, {
      "category": ["ORG"],
      "count": 4,
      "name": "Congress",
      "sentiment": {"negative": 3, "verypositive": 1}
    }, {"category": ["GPE"], "count": 1, "name": "States", "sentiment": {"verypositive": 1}}, {
      "category": ["GPE"],
      "count": 2,
      "name": "the United States",
      "sentiment": {"negative": 1, "positive": 1}
    }, {"category": ["NORP"], "count": 1, "name": "American", "sentiment": {"positive": 1}}, {
      "category": ["LAW"],
      "count": 1,
      "name": "Constitution",
      "sentiment": {"positive": 1}
    }, {"category": ["ORG"], "count": 1, "name": "Executive", "sentiment": {"verynegative": 1}}],
    "failed": 0,
    "sentimentFields": "text",
    "processed": 1,
    "requestId": "7f6977a2-fbba-49ce-b69c-bfd37ee8dad0",
    "type": "general"
  }
};

export default mockResponse;