import React from "react";
import { connect } from "react-redux";
import { Wizard } from "patternfly-react";

import { reset } from "redux-form";

import ServiceTile from "../../ServiceTile/components/ServiceTile"
import SentimentWizardInfo from "./SentimentWizardInfo";
import SentimentWizardUploadData from "./SentimentWizardUploadData";
import SentimentWizardResults from "./SentimentWizardResults";

import "./SentimentCard.css"
import { createSentiment } from "../actions";


class SentimentCard extends React.Component {
  state = {
    showModal: false,
    showLoading: false,
    activeStepIndex: 0
  };

  cardClick = e => {
    e.preventDefault();
    this.openWizard();
  };

  openWizard = () => {
    this.props.resetWizard();
    this.setState({
      showModal: true,
      activeStepIndex: 0
    });
  };

  closeWizard = () => {
    this.setState({showModal: false})
  };

  disableWizardNext = () => {
    let formErrors = this.props.wizardForm ? this.props.wizardForm.syncErrors : {};

    switch (this.state.activeStepIndex) {
      case 1:
        return formErrors && Object.keys(formErrors).length !== 0;
      default:
        return false;
    }
  };

  onWizardStepChange = index => {
    switch (index) {
      case 2:
        if (this.state.activeStepIndex === 1) {
          this.props.requestNewSentiment(
            this.props.wizardForm.values.sentimentText
          );
          this.setState({activeStepIndex: index});
        }
        break;
      default:
        this.setState({activeStepIndex: index});
    }
  };

  render() {
    const {showModal, activeStepIndex} = this.state;
    const wizardNextDisabled = this.disableWizardNext();
    const wizardSteps = [
      {
        title: "General Information", render: () => (
          <SentimentWizardInfo/>
        )
      },
      {
        title: "Experiment", render: () => (
          <SentimentWizardUploadData />
        )
      },
      {
        title: "Results", render: () => (
          <SentimentWizardResults
            sentimentError={this.props.sentimentError}
            sentimentLoading={this.props.sentimentLoading}
            sentiment={this.props.sentiment}
            sentimentText={this.props.wizardForm.values.sentimentText}
            sentimentResponse={this.props.sentimentResponse}
            sentimentAnalysisUrl={this.props.status.sentimentAnalysisUrl}
          />
        )
      }
    ];

    return (
      <div>
        <ServiceTile
          key="tile-sentiment"
          title="Sentiment Analysis"
          featured={true}
          iconClass="fa fa-thumbs-up"
          description="Quantify subjective evaluation and emotional state."
          onClick={this.cardClick}
        />
        <Wizard.Pattern
          className="sentiment-wizard service-wizard"
          show={showModal}
          backdrop="static"
          onHide={this.closeWizard}
          onExited={this.closeWizard}
          title={<span className="modal-title"><span className="service-wizard-title-icon fa fa-thumbs-up"/> Sentiment Analysis</span>}
          nextStepDisabled={wizardNextDisabled}
          steps={wizardSteps}
          onStepChanged={this.onWizardStepChange}
          loading={false}
          activeStepIndex={activeStepIndex}
        />
      </div>
    );
  };
}

function mapStateToProps(state) {
  return {
    ...{wizardForm: state.form.SentimentWizardUploadData},
    ...state.sentimentReducer,
    ...state.statusReducer
  };
}

function mapDispatchToProps(dispatch) {
  return {
    requestNewSentiment: (sentimentData) => {
      dispatch(createSentiment({
        blocking: true,
        result: true
      }, {
        "sentimentFields": "text",
        "discardData": true,
        "type": "general",
        "data": {
          "text": sentimentData
        }
      }));
    },
    resetWizard: () => {
      dispatch(reset("SentimentWizardUploadData"));
      dispatch(reset("sentimentWizardResults"));
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SentimentCard);
