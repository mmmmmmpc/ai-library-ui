import * as React from "react";
import { connect } from "react-redux";
import urljoin from "url-join";

import { Grid } from "patternfly-react";
import { generateSampleCurl } from "../utilities";
import CodeSample from "../../CodeSample/components/CodeSample"
import JsonSample from "../../JsonSample/components/JsonSample"

const sampleResponse = (
  {
  sentiment: [
    {
      category: [
        "ORG"
      ],
      count: 1,
      name: "Red Hat",
      sentiment: {
        positive: 1
      }
    }
  ],
  failed: 0,
  sentimentFields: "text",
  processed: 1,
  requestId: "a6299d58-ca0e-4004-99c5-9fbb4241870b",
  type: "general"
});

class SentimentWizardInfo extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showRequest: true
    }
  }


  render() {
    const {status} = this.props;
    const docsUrl = urljoin(status.sentimentAnalysisUrl, "/docs/index.html");
    const sampleCurl = generateSampleCurl(status.sentimentAnalysisUrl, "I love Red Hat");

    return (
      <div className="sentiment-wizard-contents sentiment-info">
        <h2>What is Sentiment Analysis?</h2>
        <p>Sentiment Analysis refers to the use of several techniques to systematically identify, extract, quantify, and
          study emotional states and subjective information. It aims to determine the attitudes, opinions, or emotional
          reactions of a speaker with respect to some topic. It can often be helpful in ascertaining the sentiment of a
          product or brand when given conversation data such as a social media feed.</p>
        <h2>The API</h2>
        <p><a href={docsUrl} target="_blank"><span>{status.sentimentAnalysisUrl}</span></a></p>
        <Grid.Row>
          <Grid.Col className="sample-request-col" lg={6}>
            <CodeSample title="Sample Request" code={sampleCurl} language="bash"/>
          </Grid.Col>
          <Grid.Col className="sample-response-col" lg={6}>
            <JsonSample title="Sample Response" object={sampleResponse}/>
          </Grid.Col>
        </Grid.Row>
      </div>)
  };
}

function mapStateToProps(state) {
  return state.statusReducer;
}

export default connect(mapStateToProps)(SentimentWizardInfo);

